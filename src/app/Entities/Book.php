<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property Category $category
 * @property Author[] $authors
 */
class Book extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['category_id', 'name'];

    /**
     * @var array
     */
    protected $hidden = ['category_id', 'created_at', 'updated_at',];

    /**
     * @var array
     */
    protected $cast = ['category_id' => 'category.name',];


    /**
     * @param int $categoryId
     * @param array $authorIds
     */
    public static function edit(self $book,int $categoryId, array $authorIds)
    {
        $book->category_id = $categoryId;
        $book->authors()->sync($authorIds);
        if (!$book->save()) {
            throw new \DomainException('can not save book');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'book_author');
    }
}
