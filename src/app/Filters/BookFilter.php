<?php

namespace App\Filters;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class BookFilter
{

    public static $defaultFilterName = 'bookName';
    private static $filterByList = [
        'bookName' => 'books.name',
        'categoryName' => 'categories.name',
        'authorName' => 'authors.name',
    ];

    public static function execute($fillterBy, $filterOrder = 'asc', $value = ''): Builder
    {
        $DBField = self::getDBField($fillterBy);
        $query = DB::table('books')
            ->select(DB::raw('books.id as bookId,
                books.name as bookName,
                categories.name as categoryName,
                group_concat(authors.name) as authorName'))
            ->leftJoin('categories', 'categories.id', '=', 'books.category_id')
            ->leftJoin('book_author', 'books.id', '=', 'book_author.book_id')
            ->leftJoin('authors', 'book_author.author_id', '=', 'authors.id')
            ->orderBy($fillterBy, $filterOrder)
            ->groupBy('bookId', 'bookName', 'categoryName');
        if (!empty($value)) {
            $query->where($DBField, 'like', '%' . $value . '%');
        }
        return $query;
    }


    private static function getDBField($fillterBy): string
    {
        if (array_key_exists($fillterBy, self::$filterByList)) {
            return self::$filterByList[$fillterBy];
        }
        throw new \RuntimeException('unsupported filter');
    }
}