<?php

namespace App\Http\Controllers\API;

use App\Entities\Book;
use App\Filters\BookFilter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $value = $request['value'];
        $fillterBy = $request['fillter_by'] ?? BookFilter::$defaultFilterName;
        $filterOrder = $request['filter_order'] ?? 'asc';
        $page = $request['page'] ?? 1;

        $query = BookFilter::execute($fillterBy, $filterOrder, $value);

        $result = $query->get();


        $paginated = new Paginator($result->forPage($page, $this->perPage), $this->perPage, $page);
        return response($paginated, $result->isEmpty() ? Response::HTTP_NO_CONTENT : Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Book::create(['name' => $request['name']]);

        return response('success', Response::HTTP_CREATED);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $categoryId = $request['category_id'];
        $authorIds = $request['author_ids'];


        Book::edit($book, $categoryId, $authorIds);

        return response('success', Response::HTTP_CREATED);
    }

}
