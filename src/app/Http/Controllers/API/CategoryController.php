<?php

namespace App\Http\Controllers\API;

use App\Entities\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $page = $request['page'] ?? 1;
        $result = Category::get();
        $paginated = new Paginator($result->forPage($page, $this->perPage), $this->perPage, $page);

        return response($paginated, $result->isEmpty() ? Response::HTTP_NO_CONTENT : Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Category::create(['name' => $request['name']]);

        return response('success', Response::HTTP_CREATED);
    }


}
