<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookAuthorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('book_author', function (Blueprint $table) {
            $table->bigInteger('book_id')->unsigned();
            $table->bigInteger('author_id')->unsigned();

            $table->primary(['book_id', 'author_id']);
            $table->foreign('book_id')->references('id')->on('books')
                ->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('authors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('book_author', function (Blueprint $table) {
            $table->dropForeign(['book_id']);
            $table->dropForeign(['author_id']);
        });

        Schema::dropIfExists('book_author');
    }
}
